﻿'Locate the file named 'NETCFv35.Messages.EN.cab' - it should be located in the directory "C:\Program Files\Microsoft.NET\SDK\CompactFramework\v3.5\WindowsCE\Diagnostics"

'Connect the device to your development PC via ActiveSync/Windows Mobile Centre V6.1 for W7.

'Copy 'NETCFv35.Messages.EN.cab' to the device (for instance in the \Windows directory).

'On the device, use the File Explorer to navigate to the file you just copied.
'Install the file by double clicking it.

' This should launch the wceload tool to perform the installation

' If Bluetooth won't connect on device then load BT Manager on device, if ZPL is not listed, stop BT & re-scan devices.

' When ZPL is available, double tap and select Trusted and Active.  If it asks if requires authentication, say NO as switched off on ZEBRa printer control panel.

' Exit Control panel and launch program on device, you should see status change on ZEBRA printer to BT connected & app will load ready to scan.

' You may need to make a new shortcut for WAFFLEPRINTER prgram on device desktop.



Imports System
Imports System.IO
Imports System.Text
Imports System.Collections

Public Class mainForm

    Dim infileHandler As Integer
    Dim outfileHandler As Integer
    Dim numReadWrite As Integer
    Dim t1 As System.Threading.Thread
    Dim stopThread As Boolean = False


    Public Sub updateMessageLog(ByVal str As String)
        If str.Length > 0 Then
            Console.WriteLine(str)
        End If
    End Sub


    Public Function connect() As Boolean
        '---port number for Bluetooth connection
        Dim inPort As Short = 2
        Dim outPort As Short = 2
        Try
            '---Opens the port for Bluetooth
            infileHandler = CreateFile("BSP" & inPort & ":", &HC0000000, 0, 0, 3, 0, 0)
            'infileHandler = CreateFile("BSP2:", CUInt(IO.FileAccess.Read Or IO.FileAccess.Write), CUInt(IO.FileShare.Read Or IO.FileShare.Write), IntPtr.Zero, 3, 0, IntPtr.Zero)
            If infileHandler <= 0 Then Throw New Exception("Unable to open BT Input Port")
            Application.DoEvents()


            'outfileHandler = CreateFile("BSP2:", CUInt(IO.FileAccess.Read Or IO.FileAccess.Write), CUInt(IO.FileShare.Read Or IO.FileShare.Write), IntPtr.Zero, 3, 0, IntPtr.Zero)
            outfileHandler = CreateFile("BSP" & outPort & ":", &HC0000000, 0, 0, 3, 0, 0)
            If outfileHandler <= 0 Then Throw New Exception("Unable to open BT Output Port")
            Application.DoEvents()

            '---invoke the thread to receive incoming messages
            stopThread = False
            t1 = New Threading.Thread(AddressOf receiveLoop)
            t1.Start()
            Return True
        Catch e As Exception
            MsgBox(e.Message, MsgBoxStyle.Critical, "Error Connecting")
            Return False
        End Try

    End Function

    Public Sub disconnect()
        stopThread = True
        CloseHandle(infileHandler)
        CloseHandle(outfileHandler)
    End Sub


    Public Function stringToByteArray(ByVal str As String) As Byte()
        '---e.g. "abcdefg" to {a,b,c,d,e,f,g}
        Dim s As Char()
        s = str.ToCharArray
        Dim b(s.Length - 1) As Byte
        Dim i As Integer
        For i = 0 To s.Length - 1
            b(i) = Convert.ToByte(s(i))
        Next
        Return b
    End Function

    Function byteArrayToString(ByVal b() As Byte) As String
        '---e.g. {a,b,c,d,e,f,g} to "abcdefg" 
        Dim str As String
        Dim enc As System.Text.ASCIIEncoding
        enc = New System.Text.ASCIIEncoding
        str = enc.GetString(b, 0, b.Length())
        Return str
    End Function


    <Runtime.InteropServices.DllImport("coredll.dll")> _
    Private Shared Function CloseHandle(ByVal hObject As Integer) As Integer
    End Function

    <Runtime.InteropServices.DllImport("coredll.dll")> _
    Private Shared Function WriteFile(ByVal hFile As Integer, _
                                  ByVal Buffer() As Byte, _
                                  ByVal nNumberOfBytesToWrite As Integer, _
                                  ByRef lpNumberOfBytesWritten As Integer, _
                                  ByVal lpOverlapped As Integer) As Boolean
    End Function


    <Runtime.InteropServices.DllImport("coredll.dll")> _
    Private Shared Function CreateFile(ByVal lpFileName As String, _
                                       ByVal dwDesiredAccess As Integer, _
                                       ByVal dwShareMode As Integer, _
                                       ByVal lpSecurityAttributes As Integer, _
                                       ByVal dwCreationDisposition As Integer, _
                                       ByVal dwFlagsAndAttributes As Integer, _
                                       ByVal hTemplateFile As Integer) As Integer
    End Function

    <Runtime.InteropServices.DllImport("coredll.dll")> _
    Private Shared Function ReadFile(ByVal hFile As Integer, _
                                ByVal Buffer() As Byte, _
                                ByVal nNumberOfBytesToRead As Integer, _
                                ByRef lpNumberOfBytesRead As Integer, _
                                ByRef lpOverlapped As Integer) As Integer
    End Function


    Public Function send(ByVal message As String) As Boolean
        '---send the message through the BT VCOM port
        Dim value As String = message
        Dim retCode As Boolean = WriteFile(outfileHandler, _
                                           stringToByteArray(value), _
                                           value.Length(), _
                                           numReadWrite, _
                                           0)
        Return retCode
    End Function

    Public Sub receiveLoop()
        '---receive the message through the serial port
        Dim inbuff(300) As Byte
        Dim retCode As Integer = ReadFile(infileHandler, _
                                          inbuff, _
                                          inbuff.Length, _
                                          numReadWrite, _
                                          0)
        Application.DoEvents()
        While True
            If retCode = 0 Or stopThread Then
                'MsgBox("Error reading message.")
                Exit While
            Else
                Dim updateDelegate As New  _
                    myDelegate(AddressOf updateMessageLog)

                updateDelegate.Invoke(byteArrayToString(inbuff))
                ReDim inbuff(300)
                retCode = ReadFile(infileHandler, _
                                   inbuff, _
                                   inbuff.Length, _
                                   numReadWrite, _
                                   0)
                Application.DoEvents()
            End If
        End While
    End Sub


    Public Delegate Sub myDelegate(ByVal str As String)

    Private Sub mainForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        disconnect()
    End Sub

    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode = System.Windows.Forms.Keys.Up) Then
            'Rocker Up
            'Up
        End If
        If (e.KeyCode = System.Windows.Forms.Keys.Down) Then
            'Rocker Down
            'Down
        End If
        If (e.KeyCode = System.Windows.Forms.Keys.Left) Then
            'Left
        End If
        If (e.KeyCode = System.Windows.Forms.Keys.Right) Then
            'Right
        End If
        If (e.KeyCode = System.Windows.Forms.Keys.Enter) Then
            'Enter
        End If

    End Sub

    Private Sub scanTextBox_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles scanTextBox.KeyDown
        If (e.KeyCode = Keys.Enter) Then
            Dim scanCode As String = scanTextBox.Text.Trim()
            If (scanCode.Length <= 0) Then
                MsgBox("Invalid Part Number!", MsgBoxStyle.Exclamation, "Validation")
            Else
                send("^XA^MD10")
                send("^PR1")
                send("~SD20")
                send("^PQ" & qtyPicker.Value.ToString())
                send("^MUd,300")
                send("^JL")
                send("^JMA")
                send("~JSB")
                send("^FO375,60^APN,36,12^FD" & scanCode.Trim() & "^FS^XZ")
                ' TEST
                ' send("^FO375,60^APN,36,12^FD" & "MB-MAV-00154/001" & "^FS^XZ")
                scanTextBox.Text = String.Empty
            End If
        End If

    End Sub


    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles connectMenuItem.Click
        If connect() Then
            disconnectMenuItem.Enabled = True
            connectMenuItem.Enabled = False
        End If
    End Sub

    Private Sub disconnectMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles disconnectMenuItem.Click
        disconnect()
        disconnectMenuItem.Enabled = False
        connectMenuItem.Enabled = True
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Me.Close()
    End Sub

    Private Sub mainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        scanTextBox.Focus()
        connect()
    End Sub
End Class
