﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class mainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mainForm))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.barcodeLabel = New System.Windows.Forms.Label
        Me.scanTextBox = New System.Windows.Forms.TextBox
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.disconnectMenuItem = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.connectMenuItem = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.qtyPicker = New System.Windows.Forms.NumericUpDown
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'barcodeLabel
        '
        Me.barcodeLabel.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold)
        Me.barcodeLabel.Location = New System.Drawing.Point(47, 81)
        Me.barcodeLabel.Name = "barcodeLabel"
        Me.barcodeLabel.Size = New System.Drawing.Size(104, 20)
        Me.barcodeLabel.Text = "Scan Barcode"
        '
        'scanTextBox
        '
        Me.scanTextBox.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular)
        Me.scanTextBox.Location = New System.Drawing.Point(47, 104)
        Me.scanTextBox.Name = "scanTextBox"
        Me.scanTextBox.Size = New System.Drawing.Size(146, 22)
        Me.scanTextBox.TabIndex = 1
        '
        'mainMenu1
        '
        Me.mainMenu1.MenuItems.Add(Me.MenuItem1)
        '
        'MenuItem1
        '
        Me.MenuItem1.MenuItems.Add(Me.MenuItem11)
        Me.MenuItem1.MenuItems.Add(Me.MenuItem12)
        Me.MenuItem1.MenuItems.Add(Me.disconnectMenuItem)
        Me.MenuItem1.MenuItems.Add(Me.MenuItem5)
        Me.MenuItem1.MenuItems.Add(Me.connectMenuItem)
        Me.MenuItem1.MenuItems.Add(Me.MenuItem8)
        Me.MenuItem1.MenuItems.Add(Me.MenuItem4)
        Me.MenuItem1.Text = "&Menu"
        '
        'MenuItem11
        '
        Me.MenuItem11.Text = "&About"
        '
        'MenuItem12
        '
        Me.MenuItem12.Text = "-"
        '
        'disconnectMenuItem
        '
        Me.disconnectMenuItem.Enabled = False
        Me.disconnectMenuItem.Text = "&Disconnect"
        '
        'MenuItem5
        '
        Me.MenuItem5.Text = "-"
        '
        'connectMenuItem
        '
        Me.connectMenuItem.Text = "&Connect"
        '
        'MenuItem8
        '
        Me.MenuItem8.Text = "-"
        '
        'MenuItem4
        '
        Me.MenuItem4.Text = "&Exit"
        '
        'qtyPicker
        '
        Me.qtyPicker.BackColor = System.Drawing.SystemColors.Info
        Me.qtyPicker.Location = New System.Drawing.Point(51, 191)
        Me.qtyPicker.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.qtyPicker.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.qtyPicker.Name = "qtyPicker"
        Me.qtyPicker.Size = New System.Drawing.Size(100, 22)
        Me.qtyPicker.TabIndex = 4
        Me.qtyPicker.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(47, 168)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 20)
        Me.Label1.Text = "Qty Labels"
        '
        'mainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(240, 268)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.qtyPicker)
        Me.Controls.Add(Me.scanTextBox)
        Me.Controls.Add(Me.barcodeLabel)
        Me.Controls.Add(Me.PictureBox1)
        Me.Menu = Me.mainMenu1
        Me.Name = "mainForm"
        Me.Text = "Norcott: Waffle Labeler"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents barcodeLabel As System.Windows.Forms.Label
    Friend WithEvents scanTextBox As System.Windows.Forms.TextBox
    Private WithEvents mainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents disconnectMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents connectMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents qtyPicker As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
